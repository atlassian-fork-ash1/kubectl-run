# Bitbucket Pipelines Pipe: Kubernetes Deploy

Run a command against a Kubernetes cluster. This pipe uses [kubectl](https://kubernetes.io/docs/reference/kubectl/overview/), a command line interface for running commands against Kubernetes clusters.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/kubectl-run:1.1.3
  variables:
    KUBE_CONFIG: '<string>'
    KUBECTL_COMMAND: '<string>'
    # RESOURCE_PATH: '<string>'
    # KUBECTL_ARGS: '<array>'
    # LABELS: '<array>'
    # WITH_DEFAULT_LABELS: '<boolean>'
    # DEBUG: '<boolean>'
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| KUBE_CONFIG (*)       | Base 64 string containing the Kubernetes configuration.  |
| KUBECTL_COMMAND (*)   | Kubectl command to run. For more details you can check the [kubectl reference guide](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands). |
| KUBECTL_ARGS          | Arguments to pass to the kubectl command. Default: `null`. |
| RESOURCE_PATH             | Path to the kubernetes spec file or a directory containing multiple spec files with your resource definitions. This option is required only if the KUBECTL_COMMAND is `apply`. Only `yaml` files are supported (files with other extensions will be ignored).|
| LABELS                | Key/value pairs that are attached to objects, such as pods. Labels are intended to be used to specify identifying attributes of objects. |
| WITH_DEFAULT_LABELS   | Whether or not to add the default labels. Check Labels added by default section for more details. |
| DEBUG                 | Turn on extra debug information. Default: `false`. If set to `true`, the pipe will add `--v=2` option to the kubectl command. If you want higher level of verbosity, you will need to pass the desired verbosity level via the `KUBECTL_ARGS` parameter. More info about the kubectl debug levels can be found [here](https://kubernetes.io/docs/reference/kubectl/cheatsheet/#kubectl-output-verbosity-and-debugging). |

_(*) = required variable._

## Labels added by default

By default, the pipe uses the following labels in order to track which pipeline created the Kubernetes resources and be able to track it from Kubernetes.

| Label | Description |
--------|-------------|
| `bitbucket.org/bitbucket_commit` | The commit hash of a commit that kicked off the build. Example: `7f777ed95a19224294949e1b4ce56bbffcb1fe9f`|
| `bitbucket.org/bitbucket_deployment_environment`| The name of the environment which the step deploys to. This is only available on deployment steps.| 
| `bitbucket.org/bitbucket_repo_owner`| The name of the owner account. |
| `bitbucket.org/bitbucket_repo_slug` | Repository name. |
| `bitbucket.org/bitbucket_build_number` | Bitbucket Pipeline number |
| `bitbucket.org/bitbucket_step_triggerer_uuid` | UUID from the user who triggered the step execution. |

## Prerequisites

- Basic knowledge is required of how Kubernetes works and how to create services and deployments on it.
- A Kubernetes cluster up and running. In order to configure the credentials in Pipelines, you will need to add your `kubeconfig` file as a repository variable (base64 encoded). This is how you can obtain it:
`cat ~/.kube/config | base64`. Just copy the base64 encoded string from stdout and put in into the KUBE_CONFIG variable in your repository.
- A docker registry (Docker Hub or similar) to store your docker image: if you are deploying to a Kubernetes cluster you will need a docker registry to store you images.


## Examples

Basic example:

```yaml
script:
  - pipe: atlassian/kubectl-run:1.1.3
    variables:
      KUBE_CONFIG: $KUBE_CONFIG
      KUBECTL_COMMAND: 'apply'
      RESOURCE_PATH: 'nginx.yml'
```

Example passing a directory with your resources definitions:

```yaml
script:
  - pipe: atlassian/kubectl-run:1.1.3
    variables:
      KUBE_CONFIG: $KUBE_CONFIG
      KUBECTL_COMMAND: 'apply'
      RESOURCE_PATH: 'my-directory/'
```

Advanced example passing KUBECTL_ARGS:

```yaml
script:
  - pipe: atlassian/kubectl-run:1.1.3
    variables:
      KUBE_CONFIG: $KUBE_CONFIG
      KUBECTL_COMMAND: 'apply'
      RESOURCE_PATH: 'nginx.yml'
      KUBECTL_ARGS:
        - '--dry-run'
```

Example unsing higher verbosity level:

```yaml
script:
  - pipe: atlassian/kubectl-run:1.1.3
    variables:
      KUBE_CONFIG: $KUBE_CONFIG
      KUBECTL_COMMAND: 'apply'
      RESOURCE_PATH: 'nginx.yml'
      KUBECTL_ARGS:
        - '--v=9'
```
## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,kubernetes
