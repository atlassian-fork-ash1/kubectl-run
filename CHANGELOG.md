# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.1.3

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.

## 1.1.2

- patch: Internal maintenance: Add check for newer version.

## 1.1.1

- patch: Fixed the bug with default labels

## 1.1.0

- minor: It is now allowed to pass a directory as a RESOURCE_PATH

## 1.0.0

- major: Changed the SPEC_FILE variable name to RESOURCE_PATH

## 0.1.4

- patch: Documentation improvements.

## 0.1.3

- patch: Fixed the docker image in the pipe.yml

## 0.1.2

- patch: Internal maintenance: increase test coverage

## 0.1.1

- patch: Fix the default label name
- patch: Fixed the default label names
- patch: Internal maintenance: Add dockerfile linter and unittest steps to the pipeline

## 0.1.0

- minor: Initial release

